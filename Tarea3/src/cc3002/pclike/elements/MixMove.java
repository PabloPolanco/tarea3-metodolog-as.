package cc3002.pclike.elements;

public class MixMove extends Movement{

	public MixMove(Ghost _ghost){
		ghost=_ghost;
	}
	@Override
	public void move() {
		double toDO=Math.random();
		if (toDO>0.5){
			new Manhattan(ghost).move();
		}
		else{
			new RandMov(ghost).move();
		}
		
	}

}
