package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import cc3002.pclike.map.PCBox;
import cc3002.pclike.pacman.GameBuilder;


public abstract class Ghost extends PCActor implements Runnable, Observer{
	
	protected int pacmanX;
	protected int pacmanY;
	protected Pacman pacman;
	protected boolean active; 	
	protected Movement move;
	protected int speed; // milliseconds
	
	public void moveTo(PCBox box){
		super.moveTo(box);
		checkPosition();
	}
	
	public abstract void moveToNextCell();
	
	public void disable(){
		active=false;
	}
	
	public void run(){
		while(active){
			try{Thread.sleep(speed);} catch(Exception e){}
			this.moveToNextCell();
		}
	}

	@Override
	public void update(Observable obs, Object args) {
		checkPosition();
		checkPosition(pacman);
	}
	
	protected void checkPosition() {
		if(pacman.totalDots!=0 && (!pacman.active || pacman.dots()==pacman.totalDots)) disable();
		if(pacman.currentBox == currentBox && pacman.active){
			pacman.lives--;
				if(pacman.lives()<=0 && pacman.active){
					pacman.active=false;
					pacman.notifyObservers();
					JOptionPane.showMessageDialog(null,"Game Over");
					System.exit(0);
				}
			pacman.reset();
			for(Ghost g: GameBuilder.ghostInGame){
				g.reset();
			}
		}
	}
	
	
	protected void checkPosition(Pacman pacman){
		pacmanX= pacman.positionX;
		pacmanY= pacman.positionY;	
	}
	
	public void initialPosition(){
		this.moveTo(initialBox);
	}
}

