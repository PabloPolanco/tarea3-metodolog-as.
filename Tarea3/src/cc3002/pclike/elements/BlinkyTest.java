package cc3002.pclike.elements;

import static org.junit.Assert.*;

import org.junit.Test;

import cc3002.pclike.pacman.GameBuilder;

public class BlinkyTest {

	Pacman _pacman=Pacman.uniqueInstancePacman();
	Blinky _blinky=Blinky.uniqueInstanceBlinky(_pacman);
	
	public GameBuilder mapTesting(){
		GameBuilder game= new GameBuilder(10,10);
		return game;
	}
	
	@Test
	public void uniqueBlinky(){
		Blinky _blinky2=Blinky.uniqueInstanceBlinky(_pacman);
		assertEquals(_blinky,_blinky2);
		assertTrue(_blinky.active);
		assertTrue(_blinky.pacman==_pacman);
		assertTrue(_blinky.speed==500);
	}
	
	@Test
	public void disableBlinky(){
		assertTrue(_blinky.active);
		_blinky.disable();
		assertFalse(_blinky.active);
		_blinky.active=true;
	}
	
	@Test
	public void checkPositionPacman() {
		_pacman.setPX(3);
		_pacman.setPY(2);
		_blinky.setPX(7);
		_blinky.setPY(5);
		_blinky.checkPosition(_pacman);
		assertTrue(_blinky.pacmanX==3);
		assertTrue(_blinky.pacmanY==2);
	}
	
	@Test
	public void moveToPacman() throws InterruptedException{
		GameBuilder game=mapTesting();
		game.addPacmanOn(1, 1);
		game.addWall(2, 1, 2, 2);
		game.addWall(1, 2, 1, 2);
		game.addGhostOn(4, 6, GameBuilder.BLINKY);
		Thread.sleep (2700);
		assertTrue(_blinky.positionX==3 && _blinky.positionY==2);
		Thread.sleep (2000);
		assertTrue(_blinky.positionX==3 && _blinky.positionY==2);
	}
}
