package cc3002.pclike.elements;


public class Manhattan extends Movement{

	public Manhattan(Ghost _ghost){
		ghost=_ghost;
	}
	
	public void move() {
		int distX= Math.abs(ghost.pacmanX-ghost.positionX);
		int distY= Math.abs(ghost.pacmanY-ghost.positionY);
		if(distY<distX)
			moveToPacmanX(ghost.positionX);
		else
			moveToPacmanY(ghost.positionY);
	}
	
	public void moveToPacmanX(int x){
		if(ghost.pacmanX<x) ghost.moveLeft();
		else ghost.moveRight();
	}
	
	public void moveToPacmanY(int y){
		if(ghost.pacmanY<y) ghost.moveUp();
		else ghost.moveDown();
	}

}