package cc3002.pclike.elements;

import java.util.Observable;

import cc3002.pclike.map.PCBox;

public abstract class PCElement extends Observable{
	protected String fileImageName;

	public PCElement(){
		fileImageName="";
	}
	
	
	public String fileImageName(){
		return fileImageName;
	}
	
	public void setImageFileName(String name){
		fileImageName=name;
	}
	
	public void moveTo(PCBox box){}

}
