package cc3002.pclike.elements;
/** 
 * Creates two uniques objects of the class PinkyAndClyde. They are a pink and a yellow ghosts who sometimes chase Pacman. 
 */ 
public class PinkyAndClyde extends Ghost{
		
	private static PinkyAndClyde uniqueInstancePinky=null;
	private static PinkyAndClyde uniqueInstanceClyde=null;
	
	private PinkyAndClyde(Pacman _pacman) {
		active = true;
		pacman = _pacman;
		speed=1000;
		move= new RandMov(this);
	}
	
	/** 
	 * Creates an unique Pinky object of PinkyAndClyde's class. If exist anyone, it returns the Pinky object that exist. 
	 * @param _pacman a pacman object that will be chased.
	 * @return The unique Pinky instance. 
	 */
	public static PinkyAndClyde uniqueInstancePinky(Pacman _pacman){
		if(uniqueInstancePinky==null){
			uniqueInstancePinky=new PinkyAndClyde(_pacman);
		}
		return uniqueInstancePinky;
	}
	
	/** 
	 * Creates an unique Clyde object of PinkyAndClyde's class. If exist anyone, it returns the Clyde object that exist. 
	 * @param _pacman a pacman object that will be chased.
	 * @return The unique Clyde instance. 
	 */
	public static PinkyAndClyde uniqueInstanceClyde(Pacman _pacman){
		if(uniqueInstanceClyde==null){
			uniqueInstanceClyde=new PinkyAndClyde(_pacman);
		}
		return uniqueInstanceClyde;
	}
	
	/** 
	 * Function that defines how this kind of ghost (both) moves in the map. 
	 * They don't chase pacman directly.
	 */
	public void moveToNextCell(){
		checkPosition();
		move.move();
	}
}
