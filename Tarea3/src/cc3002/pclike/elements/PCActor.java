package cc3002.pclike.elements;

import cc3002.pclike.map.PCBlock;
import cc3002.pclike.map.PCBox;

public abstract class PCActor extends PCElement{
	
	protected int positionX;
	protected int positionY;
	protected int initialX;
	protected int initialY;
	protected PCBox initialBox;
	protected PCBox currentBox;
	
	public PCActor(){
		currentBox=new PCBlock();
	}
	
	
	public void moveTo(PCBox box){
		currentBox.remove(this);
		currentBox=box;
		currentBox.add(this);
		
	}
	
	public void setPX(int x){
		positionX=x;
	}
	
	public void setPY(int y){
		positionY=y;
	}
	
	public void setInitialBox(){
		initialBox=currentBox;
		initialX=positionX;
		initialY=positionY;
	}
	
	public void reset(){
		this.moveTo(initialBox);
		positionX=initialX;
		positionY=initialY;
	}
	
	public void moveUp() { if(!currentBox.showTop().isBlock()){ setPY(--positionY); currentBox.moveUp(this);}}
	public void moveDown() { if(!currentBox.showDown().isBlock()){setPY(++positionY);currentBox.moveDown(this);}}
	public void moveLeft() { if(!currentBox.showLeft().isBlock()){setPX(--positionX);currentBox.moveLeft(this); }}
	public void moveRight() { if(!currentBox.showRight().isBlock()){ setPX(++positionX);currentBox.moveRight(this);}}

	

}
