package cc3002.pclike.elements;

public class RandMov extends Movement{

		public RandMov(Ghost _ghost){
			ghost=_ghost;
		}
		
		public void move() {
			double random=Math.random();
			if(random<0.25) ghost.moveDown();
			else if(random <0.5) ghost.moveLeft();
			else if(random <0.75) ghost.moveRight();
			else ghost.moveUp();
		}


}
