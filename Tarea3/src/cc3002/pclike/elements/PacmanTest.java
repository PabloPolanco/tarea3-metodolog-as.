package cc3002.pclike.elements;


import static org.junit.Assert.*;


import org.junit.Test;

import cc3002.pclike.pacman.GameBuilder;

public class PacmanTest {
	
	Pacman _pacman=Pacman.uniqueInstancePacman();

	public GameBuilder mapTesting(){
		GameBuilder game= new GameBuilder(10,10);
		return game;
	}
	
	@Test
	public void uniquePacman() {
		assertTrue(_pacman.active);
		Pacman _pacman2=Pacman.uniqueInstancePacman();
		assertEquals(_pacman,_pacman2);
	}
	
	@Test
	public void setPacmanPosition(){
		_pacman.setPX(3);
		_pacman.setPY(5);
		assertTrue(_pacman.showPX()==3 && _pacman.showPY()==5);
	}
	
	@Test
	public void eatPacdots(){
		assertTrue(_pacman.dots()==0);
		_pacman.eatAdot();
		assertTrue(_pacman.dots()==1);
		_pacman.eatAdot();
		_pacman.eatAdot();
		_pacman.eatAdot();
		_pacman.eatAdot();
		assertFalse(_pacman.dots()!=5);
		_pacman.setDots(0);
		assertTrue(_pacman.dots()==0);
	}
	
	@Test
	public void moveToPacman(){
		GameBuilder game=mapTesting();
		game.addPacmanOn(3, 5);
		_pacman.moveUp();
		_pacman.moveUp();
		_pacman.moveUp();
		_pacman.moveUp();
		_pacman.moveUp();
		_pacman.moveLeft();
		_pacman.moveDown();
		_pacman.moveDown();		
		_pacman.moveRight();
		_pacman.moveRight();
		assertTrue(_pacman.showPX()==6 && _pacman.showPY()==3);
		game.addWall(3, 7, 3, 7);
		game.addWall(3, 5, 3, 5);
		game.addWall(4, 6, 4, 6);
		_pacman.moveRight();
		_pacman.moveLeft();
		_pacman.moveDown();
		assertTrue(_pacman.showPX()==6 && _pacman.showPY()==3);
	}
}
