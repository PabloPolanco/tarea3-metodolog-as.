package cc3002.pclike.elements;

/** 
 * Creates an object of the class Blinky. It is a blue ghost who sometimes chases Pac-man. 
 */ 
public class Inky extends Ghost {

	private static Inky uniqueInstanceInky=null;
	
	private Inky(Pacman _pacman) {
		active = true;
		pacman = _pacman;
		speed=1200;
		move= new MixMove(this);
	}
	
	/** 
	 * Creates an unique Inky object. If exist anyone, it returns the Inky object that exist. 
	 * @param _pacman a pacman object that will be chased.
	 * @return The unique Inky instance. 
	 */
	public static Inky uniqueInstanceInky(Pacman _pacman){
		if(uniqueInstanceInky==null){
			uniqueInstanceInky=new Inky(_pacman);
		}
		return uniqueInstanceInky;
	}
	
	/** 
	 * Function that defines how this kind of ghost moves in the map.
	 * Half of the time Inky chases pacman.
	 */
	public void moveToNextCell(){
		move.move();
		
	}

}
