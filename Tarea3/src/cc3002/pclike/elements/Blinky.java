package cc3002.pclike.elements;

/** 
 * Creates an object of the class Blinky. It is a red ghost who chases Pac-man. 
 * Blinky receives a speed boost after a number of pac-dots have been cleared.
 */ 
public class Blinky extends Ghost {
	
	private static Blinky uniqueInstanceBlinky=null;
	
	private Blinky(Pacman _pacman) {
		active = true;
		pacman = _pacman;
		speed=500;
		move=new Manhattan(this);
	}
	
	/** 
	 * Creates an unique Blinky object. If exist anyone, it returns the Inky object that exists. 
	 * @param _pacman a pacman object that will be chased.
	 * @return The unique Blinky instance. 
	 */
	public static Blinky uniqueInstanceBlinky(Pacman _pacman){
		
		if(uniqueInstanceBlinky==null){
			uniqueInstanceBlinky=new Blinky(_pacman);
		}
		return uniqueInstanceBlinky;
	}
	
	/** 
	 * Function that defines how this kind of ghost moves in the map. 
	 * Blinky receives a speed boost after a number of pac-dots have been cleared.
	 */
	public void moveToNextCell(){
		checkPosition();
		checkPosition(pacman);
		if(pacman.dots()>8) speed=350;
		move.move();
	}
}
