package cc3002.pclike.elements;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

 
/** 
 * Creates objects of the class Pacdot.This class represents pac-pellets which ones will be eaten by pacman. 
 */ 
public class Pacdot extends PCActor implements Observer{
	
	protected int totalDots;
	protected int pacmanX;
	protected int pacmanY;
	protected int value;
	protected Pacman pacman;
	private boolean active=true;

	/** 
	 * Creates an objects of the class Pacdot.
	 * @param _pacman a pacman object that will be chased. 
	 * @param total numbers of pacdots in a map.
	 */
	public Pacdot(Pacman _pacman,int n,int valuePacdot){
		pacman=_pacman;
		totalDots=n;
		value=valuePacdot;
	}

	/** 
	 * Check if pacman ate all pacdots in the map and desactive the pacdot where pacman is.
	 */
	public void checkPacman(){
		if(active && currentBox==pacman.currentBox){
			pacman.eatAdot();
			if(pacman.dots()==totalDots){
				currentBox.remove(this);	
				pacman.active=false;
				pacman.score= pacman.score+ 1000*pacman.lives;
				pacman.notifyObservers();
				JOptionPane.showMessageDialog( null , " Congratulations ! You Won! "+"\n"+ "Your Score: "+ pacman.score ) ;
				System.exit(0) ;
			}
			active=false;
			checkPosition(pacman);
			currentBox.remove(this);		
		}
	}	

	
	protected void checkPosition(Pacman pacman){
		pacmanX= pacman.positionX;
		pacmanY= pacman.positionY;	
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		checkPacman();		
	}
}
