package cc3002.pclike.elements;

import static org.junit.Assert.*;

import org.junit.Test;

import cc3002.pclike.pacman.GameBuilder;

public class PinkyAndClydeTest {

	Pacman _pacman=Pacman.uniqueInstancePacman();
	PinkyAndClyde _pinky=PinkyAndClyde.uniqueInstancePinky(_pacman);
	PinkyAndClyde _clyde=PinkyAndClyde.uniqueInstanceClyde(_pacman);
	
	public GameBuilder mapTesting(){
		GameBuilder game= new GameBuilder(10,10);
		return game;
	}
	
	@Test
	public void uniquePinky(){
		PinkyAndClyde _pinky2=PinkyAndClyde.uniqueInstancePinky(_pacman);
		assertEquals(_pinky,_pinky2);
		assertTrue(_pinky.active);
		assertTrue(_pinky.pacman==_pacman);
		assertTrue(_pinky.speed==1000);
	}
	
	@Test
	public void uniqueClyde(){
		PinkyAndClyde _clyde2=PinkyAndClyde.uniqueInstanceClyde(_pacman);
		assertEquals(_clyde,_clyde2);
		assertTrue(_clyde.active);
		assertTrue(_clyde.pacman==_pacman);
		assertTrue(_clyde.speed==1000);
	}
	
	@Test
	public void checkPositionPacman() {
		_pacman.setPX(3);
		_pacman.setPY(2);
		_pinky.setPX(7);
		_pinky.setPY(5);
		_pinky.checkPosition(_pacman);
		assertTrue(_pinky.pacmanX==3);
		assertTrue(_pinky.pacmanY==2);
		_clyde.setPX(5);
		_clyde.setPY(9);
		_clyde.checkPosition(_pacman);
		assertTrue(_clyde.pacmanX==3);
		assertTrue(_clyde.pacmanY==2);
	}
	
	@Test
	public void moveToPacman() throws InterruptedException{
		GameBuilder game=mapTesting();
		game.addPacmanOn(1, 1);
		game.addWall(7, 7, 10, 7);
		game.addWall(6, 6, 6, 10);
		game.addWall(7, 3, 10, 3);
		game.addWall(6, 1, 6, 4);
		game.addGhostOn(9, 9, GameBuilder.PINKY);
		game.addGhostOn(2, 9, GameBuilder.CLYDE);
		Thread.sleep (1500);
		assertTrue(_pinky.positionX!=9 || _pinky.positionY!=9);
		assertTrue(_clyde.positionX!=2 || _clyde.positionY!=9);
	}
}
