package cc3002.pclike.elements;

import static org.junit.Assert.*;

import org.junit.Test;

import cc3002.pclike.pacman.GameBuilder;

public class InkyTest {

	Pacman _pacman=Pacman.uniqueInstancePacman();
	Inky _inky=Inky.uniqueInstanceInky(_pacman);
	
	public GameBuilder mapTesting(){
		GameBuilder game= new GameBuilder(10,10);
		return game;
	}
	
	@Test
	public void uniqueInky(){
		Inky _inky2=Inky.uniqueInstanceInky(_pacman);
		assertEquals(_inky,_inky2);
		assertTrue(_inky.active);
		assertTrue(_inky.pacman==_pacman);
		assertTrue(_inky.speed==1200);
	}
	
	@Test
	public void checkPositionPacman() {
		_pacman.setPX(3);
		_pacman.setPY(2);
		_inky.setPX(7);
		_inky.setPY(5);
		_inky.checkPosition(_pacman);
		assertTrue(_inky.pacmanX==3);
		assertTrue(_inky.pacmanY==2);
	}
	
	@Test
	public void moveToPacman() throws InterruptedException{
		GameBuilder game=mapTesting();
		game.addPacmanOn(1, 1);
		game.addWall(7, 7, 10, 7);
		game.addWall(6, 6, 6, 10);
		game.addGhostOn(9, 9, GameBuilder.INKY);
		Thread.sleep (1300);
		assertTrue(_inky.positionX!=9 || _inky.positionY!=9);
	}
}