package cc3002.pclike.elements;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import cc3002.pclike.map.PCBox;

/** 
 * Creates an objects of the class Pacman. This is the principal subject of the game.
 */
public class Pacman extends PCActor implements KeyListener{
	
	private int eatDots=0;
	public int totalDots;
	public int lives;
	public int score;
	public boolean active=false;
	private int pacSteps;
	private String[][] move = {{"pacmanUP1.png","pacmanUP2.png","pacmanUP1.png","pacmanUP0.png"},{"pacmanDOWN1.png","pacmanDOWN2.png","pacmanDOWN1.png","pacmanDOWN0.png"},
								{"pacmanLEFT1.png","pacmanLEFT2.png","pacmanLEFT1.png","pacmanLEFT0.png"},{"pacmanRIGHT1.png","pacmanRIGHT2.png","pacmanRIGHT1.png","pacmanRIGHT0.png"}};
	private int image=0;
	private static Pacman uniqueInstanceJPacman=null;
	
	private Pacman(){
		active=true;
		lives=3;
		score=0;
		pacSteps=-1;
	}
	
	/** 
	 * Creates an unique object of class Pacman.
	 */
	public static Pacman uniqueInstancePacman(){
		if(uniqueInstanceJPacman==null){
			uniqueInstanceJPacman=new Pacman();
		}
		return uniqueInstanceJPacman;
	}
	
	/** 
	 * Moves the pacman to a specific box.
	 * @param box an space in the map.
	 */
	public void moveTo(PCBox box){
		super.moveTo(box);
		pacSteps++;
		setChanged();
		if(this.hasChanged())
		notifyObservers();
		
	}
	
	@Override
	public void reset(){
		setImageFileName("pacmanRIGHT1.png");
		image=0;
		super.reset();
		pacSteps--;
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		switch(arg0.getKeyCode()){
		case KeyEvent.VK_UP : setImageFileName(move[0][image%4]); image++ ;moveUp(); break;
		case KeyEvent.VK_DOWN: setImageFileName(move[1][image%4]); image++ ;moveDown(); break;
		case KeyEvent.VK_LEFT: setImageFileName(move[2][image%4]); image++ ; moveLeft(); break;
		case KeyEvent.VK_RIGHT: setImageFileName(move[3][image%4]); image++ ; moveRight(); break;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {		
	}
	
	/** 
	 * Return the number of pacdots that pacman has eaten.
	 */
	public int dots() {
		setChanged();
		return eatDots;
	}
	/** 
	 * Return the number of lives of pacman .
	 */
	public int lives(){
		setChanged();
		return lives;
	}
	/** 
	 * Return where pacman is in the map. Only X coordinate.
	 */
	public int showPX(){
		return positionX;
	}
	
	/** 
	 * Return where pacman is in the map. Only Y coordinate.
	 */
	public int showPY(){
		return positionY;
	}
	
	/** 
	 * Add 1 to the number of pacdots that had been ate.
	 */
	public void eatAdot(){
		eatDots++;
		score= score + 1000/pacSteps;
	}
	
	/** 
	 * Set number of pacdots eat by pacman to 0. This function is for test coverage.
	 */
	public void setDots(int x){
		eatDots=x;
	}
}

