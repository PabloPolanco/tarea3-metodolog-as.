package cc3002.pclike.pacman;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class MainClass {
	public static void main(String[] args) throws InterruptedException {
		String mapRef="";
		System.out.println("(0) Mapa 0:"+ String.format("%26s", "(1) Mapa 1:")+String.format("%26s", "(2) Mapa 2:")+ String.format("%26s", "(3) Mapa 3:")+ String.format("%26s", "(4) Mapa 4:"));
		System.out.println("Tamaño: 10x10"+ String.format("%26s", "Tamaño: 15x15")+ String.format("%26s", "Tamaño: 20x20")+ String.format("%26s", "Tamaño: 25x25")+ String.format("%26s", "Tamaño: 21x23"));
		System.out.println("Número de puntos: 20"+ String.format("%27s", "Número de puntos: 100")+ String.format("%26s", "Número de puntos: 160")+ String.format("%26s", "Número de puntos: 250")+ String.format("%26s", "Número de puntos: 233"));
	    System.out.print( "Escriba el número del mapa que quiere jugar: " ); 
	    try { 
	      BufferedReader entrada = 
	        new BufferedReader(new InputStreamReader(System.in));   
	      mapRef = entrada.readLine().trim();
	    }
	    catch (IOException e) {}
		GameBuilder.gameMap(mapRef);
	}
}