package cc3002.pclike.pacman;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

import cc3002.pclike.elements.*;
import cc3002.pclike.map.*;

public class GameBuilder {
	private PCMap map;
	private int n;
	private int m;
	private int[][] walls;
	private Pacman pacman;
	public static ArrayList<Ghost> ghostInGame;
	public static final String BLINKY="red-ghost.png";
	public static final String INKY="blue-ghost.png";
	public static final String PINKY="pink-ghost.png";
	public static final String CLYDE="yellow-ghost.png";
	
	public GameBuilder(int _n,int _m){
		n=_n;
		m=_m;
		pacman=Pacman.uniqueInstancePacman();
		map=new PCMap(n,m, pacman);
		walls= new int[n][m];
		pacman.addObserver(map);
		ghostInGame=new ArrayList<Ghost>();
	}
	
	
	public void addBlockOn(int x,int y){
		PCBlock block =new PCBlock();
		map.setBox(x,y,block);
		walls[x-1][y-1]=1;
		
	}
	
	public void addPacDots(int x){
		addPacDot(x,"pacdot.png",1000);
	}
	
	public void addPacDot(int total, String imageFileName,int value){
		double x;
		double y;
		pacman.totalDots=total;
		for(int i=0;i<total;i++){
			x=Math.rint(Math.random()*(n-1))+1;
			y=Math.rint(Math.random()*(m-1))+1;	
			while(walls[(int) x-1][(int) y-1]==1){
				x=Math.rint(Math.random()*(n-1))+1;
				y=Math.rint(Math.random()*(m-1))+1;
			}
			Pacdot pacDot=new Pacdot(pacman,total,value);
			addActorOn(pacDot,(int)x,(int)y,imageFileName);
			pacman.addObserver(pacDot);		
		}
	}
	
	public void addPacmanOn(int x,int y){
		addNoPacdot(x-1,y-1,x+1,y+1);
		addPacmanOn(x,y,"pacmanRIGHT1.png");
	}
	
	public void addPacmanOn(int x,int y,String imageFileName){
		addActorOn(pacman,x,y,imageFileName);
	}
	
	private void addActorOn(PCActor actor, int x,int y, String imageFileName){
		walls[x-1][y-1]=1;
		actor.setImageFileName(imageFileName);
		actor.setPX(y);
		actor.setPY(x);
		actor.moveTo(map.getBox(x,y));
		actor.setInitialBox();
	}
	
	
	public void addGhostOn(int x,int y, String imageFileName){
		Ghost ghost=null;
		if(imageFileName==PINKY){
			ghost= PinkyAndClyde.uniqueInstancePinky(pacman);
		}
		else if(imageFileName==CLYDE){ 
			ghost= PinkyAndClyde.uniqueInstanceClyde(pacman);
		}
		else if(imageFileName==INKY){ 
			ghost= Inky.uniqueInstanceInky(pacman);
		}
		else if(imageFileName==BLINKY){ 
			ghost= Blinky.uniqueInstanceBlinky(pacman);
		}
		pacman.addObserver(ghost);
		ghostInGame.add(ghost);
		addActorOn(ghost,x,y,imageFileName);
		(new Thread(ghost)).start();
		
	}
	
	
	public void addWall(int xo, int yo, int xf,int yf){
		for(int x=xo;x<=xf;x++){
			for(int y=yo;y<= yf;y++){
				addBlockOn(x, y);
			}			
		}
	}
	
	public void addBorderOn(int x, int y){
		int left=0,right=0,up=0,down=0;
		Color color= new Color(0x2222FF);
		if (map.map(x,y-1).isBlock() && !map.map(x, y).isBlock()){
			left= 2;
		}
		if (map.map(x,y+1).isBlock() && !map.map(x, y).isBlock()){
			right=2;
		}
		if (map.map(x-1,y).isBlock() && !map.map(x, y).isBlock()){
			up=2;
		}
		if (map.map(x+1,y).isBlock() && !map.map(x, y).isBlock()){
			down=2;
		}
		map.map(x,y).setBorder(BorderFactory.createMatteBorder(up, left, down, right,color));
	}
	
	public void addNoPacdot(int xo, int yo, int xf, int yf){
		for(int x=xo;x<=xf;x++){
			for(int y=yo;y<= yf;y++){
				if(x!=0 && y!=0)
					walls[x-1][y-1]=1;
			}
		}
	}
	
	public void addBorder(){
		for(int x=1;x<=n;x++){
			for(int y=1;y<= m;y++){
				addBorderOn(x, y);
			}			
		}
	}

	
	public void buildAndOpen(){
		map.loadBoxes();
		JFrame frame=map.openInWindow();
		frame.setFocusable(true);
		frame.setResizable(false);
		frame.setSize(map.getPreferredSize());
		frame.addKeyListener(pacman);
		map.updateUI();
	}
		
	public static GameBuilder mapCreation1(){
		GameBuilder game1=new GameBuilder(15,15);	
		game1.addWall(8,3, 8,5);
		game1.addWall(8,11, 8,13);
		game1.addWall(3,8, 5, 8);
		game1.addWall(11,8, 13, 8);
		game1.addWall(1,1,1,1);
		game1.addWall(1,3,1,3);
		game1.addWall(1,5, 1,5);
		game1.addWall(1,7, 1,7);
		game1.addWall(1,9, 1,9);
		game1.addWall(1,11, 1,11);
		game1.addWall(1,13, 1,13);
		game1.addWall(1,15, 1,15);
		game1.addWall(3,1,3,1);
		game1.addWall(5,1,5,1);
		game1.addWall(7,1,7,1);
		game1.addWall(9,1,9,1);
		game1.addWall(11,1,11,1);
		game1.addWall(13,1,13,1);
		game1.addWall(15,1,15,1);
		game1.addWall(1,15, 1,15);
		game1.addWall(3,15, 3,15);
		game1.addWall(5,15, 5,15);
		game1.addWall(7,15, 7,15);
		game1.addWall(9,15, 9,15);
		game1.addWall(11,15, 11,15);
		game1.addWall(13,15, 13,15);
		game1.addWall(15,15, 15,15);
		game1.addWall(15,3,15,3);
		game1.addWall(15,5,15,5);
		game1.addWall(15,7,15,7);
		game1.addWall(15,9,15,9);
		game1.addWall(15,11,15,11);
		game1.addWall(15,13,15,13);
		game1.addWall(3,3,3,3);
		game1.addWall(4,4,4,4);
		game1.addWall(5,5,5,5);
		game1.addWall(6,6,6,6);
		game1.addWall(7,7,7,7);
		game1.addWall(9,9,9,9);
		game1.addWall(10,10,10,10);
		game1.addWall(11,11,11,11);
		game1.addWall(12,12,12,12);
		game1.addWall(13,13,13,13);
		game1.addWall(13,3,13,3);
		game1.addWall(12,4,12,4);
		game1.addWall(11,5,11,5);
		game1.addWall(10,6,10,6);
		game1.addWall(9,7,9,7);
		game1.addWall(7,9,7,9);
		game1.addWall(6,10,6,10);
		game1.addWall(5,11,5,11);
		game1.addWall(4,12,4,12);
		game1.addWall(3,13,3,13);
		game1.addBorder();
		return game1;
	}
	
	public static GameBuilder mapCreation2(){
		GameBuilder game2=new GameBuilder(20,20);
		game2.addWall(2,2, 3,3);
		game2.addWall(2,5, 3,10);
		game2.addWall(2,12, 3,16);
		game2.addWall(2,18, 3,19);
		game2.addWall(5,18, 9,19);
		game2.addWall(11,18, 16,19);
		game2.addWall(18,18, 19,19);
		game2.addWall(18,11, 19,16);
		game2.addWall(18,5, 19,9);
		game2.addWall(18,2, 19,3);
		game2.addWall(12,2, 16,3);
		game2.addWall(5,2, 10,3);
		game2.addWall(7, 4, 7, 4);
		game2.addWall(14, 4, 14, 4);
		game2.addWall(7, 17, 7, 17);
		game2.addWall(14, 17, 14, 17);
		game2.addWall(5,5, 6,6);
		game2.addWall(5,8, 6,13);
		game2.addWall(5,15, 6,16);
		game2.addWall(8,15, 10,16);
		game2.addWall(12,15, 13,16);
		game2.addWall(15,15, 16,16);
		game2.addWall(15,8, 16,13);
		game2.addWall(15,5, 16,6);
		game2.addWall(11,5, 13,6);
		game2.addWall(8,5, 9,6);
		game2.addWall(8,8, 8,13);
		game2.addWall(13,8, 13,13);
		game2.addWall(10,8, 11,9);
		game2.addWall(10,12, 11,13);
		game2.addBorder();
		return game2;
	}
		
	public static GameBuilder mapCreation3(){
		GameBuilder game3=new GameBuilder(25,25);
		game3.addWall(1,1, 1,1);
		game3.addWall(1,6, 1,6);
		game3.addWall(3,4, 3,4);
		game3.addWall(2,3, 15,3);
		game3.addWall(1,4 ,2,5);
		game3.addWall(16,4 ,16,4);
		game3.addWall(17,5 ,17,5);
		game3.addWall(18,6 ,18,6);
		game3.addWall(19,7 ,19,7);
		game3.addWall(20,8 ,20,20);
		game3.addWall(19,21 ,19,21);
		game3.addWall(18,22 ,18,22);
		game3.addWall(6,23 ,17,23);
		game3.addWall(5,22 ,5,22);
		game3.addWall(4,21 ,4,21);
		game3.addWall(3,8 ,3,20);
		game3.addWall(4,7 ,4,7);
		game3.addWall(5,6 ,14,6);
		game3.addWall(15,7 ,15,7);
		game3.addWall(16,8 ,16,8);
		game3.addWall(17,9 ,17,19);
		game3.addWall(7,20 ,16,20);
		game3.addWall(6,10 ,6,19);
		game3.addWall(7,9 ,7,11);
		game3.addWall(8,9 ,8,10);
		game3.addWall(9,9 ,11,9);
		game3.addWall(12,9 ,12,10);
		game3.addWall(13,9 ,13,11);
		game3.addWall(14,10 ,14,14);
		game3.addWall(13,15 ,13,15);
		game3.addWall(10,16 ,12,16);
		game3.addWall(11,17 ,11,17);
		game3.addWall(9,15 ,9,15); /* Spiral */
		game3.addWall(17,1 ,21,1);
		game3.addWall(18,2 ,21,2);
		game3.addWall(19,3 ,21,3);
		game3.addWall(20,4 ,21,4);
		game3.addWall(21,5 ,21,5);
		game3.addWall(25,6 ,25,25);
		game3.addWall(24,7 ,24,25);
		game3.addWall(23,8 ,23,25);
		game3.addWall(22,22 ,22,25);
		game3.addWall(21,23 ,21,25);
		game3.addWall(20,24 ,20,25);
		game3.addWall(19,25 ,19,25);
		game3.addWall(16,17 ,16,17);
		game3.addWall(14,18 ,14,19);
		game3.addWall(13,19 ,13,19);
		game3.addNoPacdot(21,1, 25,8);
		game3.addNoPacdot(15,17, 16,19);
		game3.addBorder();
		return game3;
	}
	
	public static GameBuilder mapCreation4(){
		GameBuilder game4 =new GameBuilder(23,21);
		game4.addWall(2, 2, 3, 4);
		game4.addWall(2, 6, 3, 9);
		game4.addWall(1, 11, 3, 11);
		game4.addWall(2, 13, 3, 16);
		game4.addWall(2, 18, 3, 20);
		game4.addWall(5, 2, 5, 4);
		game4.addWall(5, 6, 10, 6);/*T invertida izq*/
		game4.addWall(5, 8, 5, 14);/*T medio arriba*/ 
		game4.addWall(5, 16, 10, 16);/*T invertida der*/
		game4.addWall(5, 18, 5, 20);
		game4.addWall(7, 1, 10, 4);
		game4.addWall(7, 6, 7, 9);
		game4.addWall(6, 11, 7, 11);
		game4.addWall(7, 13, 7, 16);
		game4.addWall(7, 18, 10, 21);
		game4.addWall(9, 8, 14, 14); 
		game4.addWall(13, 1, 16, 4);
		game4.addWall(13, 6, 16, 6);
		game4.addWall(16, 8, 16, 14);/*T medio abajo*/
		game4.addWall(16, 11, 18, 11);		
		game4.addWall(13, 16, 16, 16);
		game4.addWall(13, 18, 16, 21);
		game4.addWall(18, 2, 18, 4);
		game4.addWall(18, 4, 20, 4);
		game4.addWall(18, 6, 18, 9);
		game4.addWall(18, 13, 18, 16);
		game4.addWall(18, 18, 18, 20);
		game4.addWall(18, 18, 20, 18);
		game4.addWall(18, 18, 20, 18);
		game4.addWall(20, 1, 20, 2);
		game4.addWall(20, 6, 22, 6);
		game4.addWall(20, 8, 20, 14);
		game4.addWall(20, 16, 22, 16);		
		game4.addWall(18, 20, 18, 21);
		game4.addWall(22, 2, 22, 9);
		game4.addWall(20, 11, 22, 11);
		game4.addWall(22, 13, 22, 20);
		game4.addNoPacdot(11,1, 12,4);
		game4.addNoPacdot(11,18, 12,21);
		game4.addBorder();
		return game4;
	}
		
	public static GameBuilder mapCreation0(){
		GameBuilder game=new GameBuilder(10,10);
		game.addWall(3,3, 3, 5);
		game.addBorder();
		return game;
	}
	
	public static void gameMap(String map){
		GameBuilder game =GameBuilder.mapCreation0();
		GameBuilder game1 =GameBuilder.mapCreation1();
		GameBuilder game2 =GameBuilder.mapCreation2();
		GameBuilder game3 =GameBuilder.mapCreation3();
		GameBuilder game4 =GameBuilder.mapCreation4();
		
		if(map.equals("1")){
			game1.addPacmanOn(8,8);
			game1.buildAndOpen();
			game1.addGhostOn(14,8,GameBuilder.BLINKY);
			game1.addGhostOn(2,8,GameBuilder.INKY);
			game1.addGhostOn(8,2,GameBuilder.PINKY);
			game1.addGhostOn(8,14,GameBuilder.CLYDE);
			game1.addPacDots(100);
		}
		else if(map.equals("2")){
			game2.addPacmanOn(1,1);
			game2.buildAndOpen();
			game2.addGhostOn(10,11,GameBuilder.BLINKY);
			game2.addGhostOn(4,17,GameBuilder.INKY);
			game2.addGhostOn(11,10,GameBuilder.PINKY);
			game2.addGhostOn(17,4,GameBuilder.CLYDE);
			game2.addPacDots(160);
		}
		else if(map.equals("3")){
			game3.addPacmanOn(1,3);
			game3.buildAndOpen();
			game3.addGhostOn(24,1,GameBuilder.BLINKY);
			game3.addGhostOn(16,19,GameBuilder.INKY);
			game3.addGhostOn(1,25,GameBuilder.PINKY);
			game3.addGhostOn(13,12,GameBuilder.CLYDE);
			game3.addPacDots(250);
		}
		else if(map.equals("4")){
			game4.addPacmanOn(15,11);
			game4.buildAndOpen();
			game4.addGhostOn(23,21,GameBuilder.BLINKY);
			game4.addGhostOn(1,1,GameBuilder.INKY);
			game4.addGhostOn(1,21,GameBuilder.PINKY);
			game4.addGhostOn(23,1,GameBuilder.CLYDE);
			game4.addPacDots(223);
		}
		else{
			game.addPacmanOn(1,1);
			game.buildAndOpen();
			game.addGhostOn(10,10,GameBuilder.BLINKY);
			game.addGhostOn(1,10,GameBuilder.INKY);
			game.addGhostOn(10,1,GameBuilder.PINKY);
			game.addGhostOn(5,5,GameBuilder.CLYDE);
			game.addPacDots(30);
		}
	}
}
