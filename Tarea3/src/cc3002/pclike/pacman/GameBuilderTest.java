package cc3002.pclike.pacman;

import static org.junit.Assert.*;

import org.junit.Test;

import cc3002.pclike.elements.Pacman;

public class GameBuilderTest {
	
	Pacman _pacman=Pacman.uniqueInstancePacman();
	GameBuilder game;
	
	@Test
	public void mapCreation0test(){
		game=GameBuilder.mapCreation0();
		game.addPacmanOn(1, 1);
		game.addPacDots(93);
		assertTrue(_pacman.dots()==0);
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveDown();
		assertTrue(_pacman.showPX()==4 && _pacman.showPY()==2);
		assertTrue(_pacman.dots()==3);
		_pacman.setDots(0);
	}
	
	@Test
	public void mapCreation1test(){
		game=GameBuilder.mapCreation1();
		game.addPacmanOn(8, 8);
		game.addPacDots(160);
		assertTrue(_pacman.dots()==0);
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveDown();
		assertTrue(_pacman.showPX()==11 && _pacman.showPY()==10);
		assertTrue(_pacman.dots()==4);
		_pacman.setDots(0);
	}
	
	@Test
	public void mapCreation2test(){
		game=GameBuilder.mapCreation2();
		game.addPacmanOn(1, 1);
		game.addPacDots(209);
		assertTrue(_pacman.dots()==0);
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveUp();
		_pacman.moveRight();
		_pacman.moveUp();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveLeft();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveLeft();
		_pacman.moveDown();
		assertTrue(_pacman.showPX()==4 && _pacman.showPY()==6);
		assertTrue(_pacman.dots()==7);
		_pacman.setDots(0);
	}
	
	@Test
	public void mapCreation3test(){
		game=GameBuilder.mapCreation3();
		game.addPacmanOn(11, 18);
		game.addPacDots(362);
		assertTrue(_pacman.dots()==0);
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveLeft();
		_pacman.moveLeft();
		_pacman.moveLeft();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveUp();
		_pacman.moveRight();
		_pacman.moveUp();
		_pacman.moveRight();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveLeft();
		_pacman.moveDown();
		_pacman.moveLeft();
		_pacman.moveUp();
		_pacman.moveUp();
		_pacman.moveLeft();
		_pacman.moveLeft();
		_pacman.moveLeft();
		_pacman.moveDown();
		_pacman.moveDown();
		_pacman.moveRight();
		_pacman.moveRight();
		_pacman.moveDown();
		System.out.println(_pacman.showPX()+" "+_pacman.showPY() +" "+_pacman.dots());
		assertTrue(_pacman.showPX()==16 && _pacman.showPY()==16);
		assertTrue(_pacman.dots()==6);
		_pacman.setDots(0);
	}
}
