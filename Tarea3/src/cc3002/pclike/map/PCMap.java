package cc3002.pclike.map;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import cc3002.pclike.elements.Pacman;


public class PCMap extends JPanel implements Observer{
	
	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;
	
	
	private PCBox[][] box;
	private int n;
	private int m;
	private JLabel lives;
	private JLabel score;
	private Pacman pacman;
	
	public PCMap(int _n,int _m,Pacman _pacman){
		box = new PCBox[_n+2][_m+2];
		for(int i=0;i<_n+2;i++)
			for(int j=0;j<_m+2;j++)
				box[i][j]=new PCBlock();
		n = _n;
		m = _m;
		pacman= _pacman;
		lives= new JLabel("Lives: "+ pacman.lives, JLabel.CENTER);
		score= new JLabel("Score: "+ 0, JLabel.CENTER);
		setLayout(new GridLayout(n,m));
		setPreferredSize(new Dimension(m*30,n*30));
		setBackground(Color.black);
		fillMatrix();
		updateNeighbors();
	}
	
	public PCBox map(int x, int y){
		return box[x][y];
	}
	
	public void setBox(int i,int j,PCBox _box){
		box[i][j]=_box;
		updateNeighbors();
		updateUI();
	}
	public PCBox getBox(int i,int j){
		return box[i][j];
	}
	
	
	public JFrame openInWindow(){
		JFrame frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(this, BorderLayout.CENTER);
		frame.getContentPane().setBackground(Color.black);
		lives.setPreferredSize(new Dimension(20, 20)); 
		score.setPreferredSize(new Dimension(20, 20)); 
		frame.getContentPane().add(lives, BorderLayout.NORTH);
		frame.getContentPane().add(score, BorderLayout.SOUTH);
		score.setFont(new Font("MONOSPACED", Font.ITALIC + Font.BOLD,20));
		lives.setFont(new Font("MONOSPACED",Font.ITALIC + Font.BOLD,20));
		lives.setForeground(Color.YELLOW);
		score.setForeground(Color.YELLOW);
		frame.setVisible(true);
		updateUI();
		return frame;
	}
	
	private void fillMatrix(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]=new PCEmptyBox();
			}
		}
	}
	
	public void loadBoxes(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				add(box[i][j]);
			}
		}
	}
	
	 public static void verticalText(JLabel l){
		  String t[]=new String[l.getText().length()];
		  String txt="<html>";
		  for(int i=0;i<t.length;i++){
		   t[i]=""+l.getText().charAt(i);
		  }		  
		  for(int i=0;i<t.length;i++){
		   txt+=t[i];
		   txt+="<br>";
		  }
		  txt+="</html>";
		  
		  l.setText(txt);
	 }
	
	public void updateLabels(){
			if (pacman.lives>0){
				lives.setText("Lives: "+pacman.lives);
				score.setText("Score: "+pacman.score);
			}
			else{
				lives.setText("Lives "+ 0);
				score.setText("Score "+ 0);
			}
	}
	private void updateNeighbors(){
		for(int i=1; i<=n;i++){
			for(int j=1;j<=m;j++){
				box[i][j]
					.updateNeighbors(
							box[i-1][j],
							box[i+1][j],
							box[i][j-1],
							box[i][j+1]);
				box[i][j].updateBox();
			}
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		updateLabels();
		
	}
}
