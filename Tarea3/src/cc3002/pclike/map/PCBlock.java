package cc3002.pclike.map;

import java.awt.Color;



public class PCBlock extends PCBox{

	/**
	 * serial version to jump eclipse warning
	 */
	private static final long serialVersionUID = 1L;

	public PCBlock(){
		super();
		setOpaque(true);
		setBackground(Color.BLACK);
		revalidate();
	}
	@Override
	public boolean isBlock() {
		return true;
	}

}
