package cc3002.pclike.map;

import java.awt.*;
import java.util.LinkedList;

import javax.swing.*;

import cc3002.pclike.elements.*;

public class PCBox extends JLabel{
	
	private LinkedList<PCElement> elements;
	
	protected PCBox topBox;
	protected PCBox downBox;
	protected PCBox leftBox;
	protected PCBox rightBox;
	
	
	public PCBox(){
		elements = new LinkedList<PCElement>();
		elements.add(new PCNullElement());
		setPreferredSize(new Dimension(24,24));
		setMaximumSize(new Dimension(24,24));
		setMinimumSize(new Dimension(24,24));
		setBackground(Color.black);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setOpaque(true);
	}
	
	public boolean isBlock(){
		return false;
	}
	
	public void moveUp(PCElement element){}
	public void moveDown(PCElement element){}
	public void moveLeft(PCElement element){}
	public void moveRight(PCElement element){}
	
	public void updateNeighbors(PCBox _top, PCBox _down,PCBox _left, PCBox _right){
		topBox = _top;
		downBox = _down;
		leftBox = _left;
		rightBox = _right;
	}
	
	public void add(PCElement newElement){
		elements.addFirst(newElement);
		updateBox();
	}
	
	public void remove(PCElement element){
		elements.remove(element);
		updateBox();
	}
	
	public void updateBox(){
		setIcon(new ImageIcon(elements.get(0).fileImageName()));
		repaint();
	}
	
	public PCBox showTop(){
		return topBox;
	}
	
	public PCBox showDown(){
		return downBox;
	}
	
	public PCBox showLeft(){
		return leftBox;
	}
	
	public PCBox showRight(){
		return rightBox;
	}
}
