package cc3002.pclike.map;


import cc3002.pclike.elements.*;

public class PCEmptyBox extends PCBox{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public void moveUp(PCElement element){
		element.moveTo(topBox);
	}
	
	public void moveDown(PCElement element){
		element.moveTo(downBox);
	}
	public void moveLeft(PCElement element){
		element.moveTo(leftBox);
	}
	
	public void moveRight(PCElement element){
		element.moveTo(rightBox);
	}
	
}
